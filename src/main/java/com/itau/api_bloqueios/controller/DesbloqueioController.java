package com.itau.api_bloqueios.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.itau.api_bloqueios.services.DesbloqueioService;

import br.com.itau.api_bloqueios.helpers.SolicitacaoBDT;

@CrossOrigin
@RestController
@RequestMapping("/desbloqueio")
public class DesbloqueioController {

	@Autowired
	DesbloqueioService desbloqueioService;
	
	@PostMapping
	public void realizarDesbloqueio(@RequestBody SolicitacaoBDT solicitacaoBDT) {
		desbloqueioService.desbloquearProduto(solicitacaoBDT);
		
	}
}
