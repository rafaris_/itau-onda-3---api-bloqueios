package com.itau.api_bloqueios.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.itau.api_bloqueios.services.TransferenciaService;

import br.com.itau.api_bloqueios.helpers.SolicitacaoBDT;

@CrossOrigin
@RestController
@RequestMapping("/transferencia")
public class TransferenciaController {
	
	@Autowired
	TransferenciaService transferenciaService;
	
	@PostMapping
	public void solicitarTransferencia(@RequestBody SolicitacaoBDT solicitacaoBDT) {
		transferenciaService.transferirValor(solicitacaoBDT);		
	}
	
}
