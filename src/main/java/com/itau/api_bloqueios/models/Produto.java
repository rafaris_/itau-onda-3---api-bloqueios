package com.itau.api_bloqueios.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="produto")
public class Produto {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_produto")
	private int idProduto;
	
	@NotNull
	@Column(name="nome_produto")
	private String nomeProduto;
	
	@NotNull
	@Column(name="prioridade_produto")
	private int prioridadeBloqueio;

	public int getIdProduto() {
		return idProduto;
	}

	public void setIdProduto(int idProduto) {
		this.idProduto = idProduto;
	}

	public String getNomeProduto() {
		return nomeProduto;
	}

	public void setNomeProduto(String nomeProduto) {
		this.nomeProduto = nomeProduto;
	}

	public int getPrioridadeBloqueio() {
		return prioridadeBloqueio;
	}

	public void setPrioridadeBloqueio(int prioridadeBloqueio) {
		this.prioridadeBloqueio = prioridadeBloqueio;
	}
	
}
