package com.itau.api_bloqueios.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="bloqueio")
public class Bloqueio {
	
	@Id
	@Column(name="numero_ordem")
	private int numeroOrdem;
	
	@NotNull
	@Column(name="cpf_reu")
	private int cpfReu;
	
	@NotNull
	@Column(name="valor_solicitado_bloqueio", columnDefinition="decimal", precision=8,scale=2)
	private double valorSolicitadoBloqueio;
	
	@NotNull
	@Column(name="valor_bloqueado", columnDefinition="decimal", precision=8,scale=2)
	private double valorBloqueado;
	
	@NotNull
	@Column(name="bloqueio_andamento")
	private boolean bloqueioAndamento;

	public int getNumeroOrdem() {
		return numeroOrdem;
	}

	public void setNumeroOrdem(int numeroOrdem) {
		this.numeroOrdem = numeroOrdem;
	}

	public int getCpfReu() {
		return cpfReu;
	}

	public void setCpfReu(int cpfReu) {
		this.cpfReu = cpfReu;
	}

	public double getValorSolicitadoBloqueio() {
		return valorSolicitadoBloqueio;
	}

	public void setValorSolicitadoBloqueio(double valorSolicitadoBloqueio) {
		this.valorSolicitadoBloqueio = valorSolicitadoBloqueio;
	}

	public double getValorBloqueado() {
		return valorBloqueado;
	}

	public void setValorBloqueado(double valorBloqueado) {
		this.valorBloqueado = valorBloqueado;
	}

	public boolean isBloqueioAndamento() {
		return bloqueioAndamento;
	}

	public void setBloqueioAndamento(boolean bloqueioAndamento) {
		this.bloqueioAndamento = bloqueioAndamento;
	}
	
	
}


