package com.itau.api_bloqueios.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table (name="produtoscliente")
public class ProdutosCliente {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_produto_cliente")
	private int idProdutoCliente;
	
	@NotNull
	@Column(name="saldo_produto")
	private double saldoProduto;
	
	@NotNull
	@Column (name="cpf_cliente")
	private int cpfCliente;

	@ManyToOne
	@JoinColumn(name="id_produto")
	private Produto produto;

	public int getIdProdutoCliente() {
		return idProdutoCliente;
	}

	public void setIdProdutoCliente(int idProdutoCliente) {
		this.idProdutoCliente = idProdutoCliente;
	}

	public double getSaldoProduto() {
		return saldoProduto;
	}

	public void setSaldoProduto(double saldoProduto) {
		this.saldoProduto = saldoProduto;
	}

	public int getCpfCliente() {
		return cpfCliente;
	}

	public void setCpfCliente(int cpfCliente) {
		this.cpfCliente = cpfCliente;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	
	
}
