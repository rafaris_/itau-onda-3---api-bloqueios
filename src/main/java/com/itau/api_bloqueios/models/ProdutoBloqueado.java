package com.itau.api_bloqueios.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;



@Entity
@Table(name="produtobloqueado")
public class ProdutoBloqueado {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_produto_bloqueado")
	private int idProdutoBloqueado;
	
	@Column(name="valor_bloqueado_produto", columnDefinition="decimal", precision=8, scale=2)
	@NotNull
	private double valorBloqueado;
	
	@OneToOne
	@JoinColumn(name="id_produto_cliente")
	private ProdutosCliente produtosCliente;
	
	@OneToOne
	@JoinColumn(name="numero_ordem")	
	private Bloqueio bloqueio;

	public int getIdProdutoBloqueado() {
		return idProdutoBloqueado;
	}

	public void setIdProdutoBloqueado(int idProdutoBloqueado) {
		this.idProdutoBloqueado = idProdutoBloqueado;
	}

	public double getValorBloqueado() {
		return valorBloqueado;
	}

	public void setValorBloqueado(double valorBloqueado) {
		this.valorBloqueado = valorBloqueado;
	}

	public ProdutosCliente getProdutosCliente() {
		return produtosCliente;
	}

	public void setProdutosCliente(ProdutosCliente produtosCliente) {
		this.produtosCliente = produtosCliente;
	}

	public Bloqueio getBloqueio() {
		return bloqueio;
	}

	public void setBloqueio(Bloqueio bloqueio) {
		this.bloqueio = bloqueio;
	}
}
