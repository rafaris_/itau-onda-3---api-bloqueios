package com.itau.api_bloqueios.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.itau.api_bloqueios.models.ProdutosCliente;


public interface ProdutosClienteRepository extends CrudRepository<ProdutosCliente, Integer> {

	public List<ProdutosCliente> findAllBycpfCliente(int cpfCliente);
	

}
