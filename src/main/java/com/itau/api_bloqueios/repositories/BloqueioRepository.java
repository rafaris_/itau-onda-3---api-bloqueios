package com.itau.api_bloqueios.repositories;

import org.springframework.data.repository.CrudRepository;

import com.itau.api_bloqueios.models.Bloqueio;

public interface BloqueioRepository extends CrudRepository<Bloqueio, Integer> {

}
