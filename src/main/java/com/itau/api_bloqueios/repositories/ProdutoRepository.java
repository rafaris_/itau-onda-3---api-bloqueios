package com.itau.api_bloqueios.repositories;

import org.springframework.data.repository.CrudRepository;

import com.itau.api_bloqueios.models.Produto;

public interface ProdutoRepository extends CrudRepository<Produto, Integer>{

}
