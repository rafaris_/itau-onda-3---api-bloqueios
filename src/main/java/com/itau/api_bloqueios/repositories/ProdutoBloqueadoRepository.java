package com.itau.api_bloqueios.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.itau.api_bloqueios.models.Bloqueio;
import com.itau.api_bloqueios.models.ProdutoBloqueado;

public interface ProdutoBloqueadoRepository extends CrudRepository<ProdutoBloqueado, Integer> {

	public List<ProdutoBloqueado> findAllByBloqueio(Bloqueio bloqueio);
	
}
