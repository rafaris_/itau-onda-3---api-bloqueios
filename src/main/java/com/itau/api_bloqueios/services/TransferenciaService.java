package com.itau.api_bloqueios.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itau.api_bloqueios.models.Bloqueio;
import com.itau.api_bloqueios.models.ProdutoBloqueado;

import br.com.itau.api_bloqueios.helpers.SolicitacaoBDT;

@Service
public class TransferenciaService {
	private double valorATransferir;
	
	@Autowired
	ProdutoBloqueadoService produtoBloqueadoService;
	
	@Autowired
	BloqueioService bloqueioService;
	
	public void transferirValor(SolicitacaoBDT solicitacaoBDT) {
		List<ProdutoBloqueado> produtosBloqueado = new ArrayList<>();

		
		valorATransferir = solicitacaoBDT.getValorSolicitado();
		Bloqueio bloqueio = bloqueioService.consultaBloqueio(solicitacaoBDT.getNumeroOrdem()).get();
		double novoValorBloqueado = bloqueio.getValorBloqueado();
		produtosBloqueado = produtoBloqueadoService.consultarBloqueio(bloqueio);
		
		for (ProdutoBloqueado produtosBloqueados : produtosBloqueado) {
			
			if(valorATransferir > 0) {
				if (valorATransferir >= produtosBloqueados.getValorBloqueado()) {
					valorATransferir -= produtosBloqueados.getValorBloqueado();
					novoValorBloqueado -= produtosBloqueados.getValorBloqueado();
					produtosBloqueados.setValorBloqueado(0);
					produtoBloqueadoService.atualizarSaldo(produtosBloqueados);	
				}else {
					produtosBloqueados.setValorBloqueado(produtosBloqueados.getValorBloqueado() - valorATransferir);
					novoValorBloqueado -= valorATransferir;
					valorATransferir -= valorATransferir;		
					produtoBloqueadoService.atualizarSaldo(produtosBloqueados);	
				}
				
			}
		}
		if (novoValorBloqueado == 0) {
			bloqueio.setBloqueioAndamento(false);
		}
		
		bloqueio.setValorBloqueado(novoValorBloqueado);		
		bloqueioService.alterarBloqueio(bloqueio);
	}
}
