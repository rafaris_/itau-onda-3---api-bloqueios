package com.itau.api_bloqueios.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itau.api_bloqueios.models.Bloqueio;
import com.itau.api_bloqueios.models.ProdutoBloqueado;
import com.itau.api_bloqueios.repositories.ProdutoBloqueadoRepository;



@Service
public class ProdutoBloqueadoService {
	@Autowired
	ProdutoBloqueadoRepository produtoBloqueadoRepository;
		
	public List<ProdutoBloqueado> consultarBloqueio(Bloqueio bloqueio) {
			
		return produtoBloqueadoRepository.findAllByBloqueio(bloqueio);	
					
	}
	public void gravarBloqueio(ProdutoBloqueado produtoBloqueado) {			
	
		produtoBloqueadoRepository.save(produtoBloqueado);
	}
	
	public void gravarListaBloqueio(List<ProdutoBloqueado> produtosBloqueado) {
		
		for (ProdutoBloqueado produtoBloqueado : produtosBloqueado) {
			produtoBloqueadoRepository.save(produtoBloqueado);
		}
		
	}
	
	public void atualizarSaldo(ProdutoBloqueado produtoBloqueado) {
		produtoBloqueadoRepository.save(produtoBloqueado);
	}	
}

