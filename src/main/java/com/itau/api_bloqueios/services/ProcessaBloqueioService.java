package com.itau.api_bloqueios.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itau.api_bloqueios.models.Bloqueio;
import com.itau.api_bloqueios.models.ProdutoBloqueado;
import com.itau.api_bloqueios.models.ProdutosCliente;

import br.com.itau.api_bloqueios.helpers.ProdutosClienteBloqueados;

@Service
public class ProcessaBloqueioService {

	@Autowired
	ProdutosClienteService produtosClienteService;
	
	
	public List<ProdutosClienteBloqueados> gerarBloqueio(List<ProdutosCliente> produtoCliente, double saldoParaBloqueio) {

		List<ProdutosClienteBloqueados> produtosClienteBloqueados = new ArrayList<>();

		for (ProdutosCliente produtosCliente : produtoCliente) {
			 ProdutosClienteBloqueados produtosBloqueadosCliente = new ProdutosClienteBloqueados();
			 
			if (saldoParaBloqueio > 0) {
				if (produtosCliente.getSaldoProduto() >= saldoParaBloqueio) {
					produtosBloqueadosCliente = copiarProdutosCliente(produtosCliente);
					double novoSaldo = produtosCliente.getSaldoProduto() - saldoParaBloqueio;
					produtosCliente.setSaldoProduto(novoSaldo);
					produtosClienteService.atualizarProdutosCliente(produtosCliente);
					produtosBloqueadosCliente.setSaldoProduto(saldoParaBloqueio);
					produtosClienteBloqueados.add(produtosBloqueadosCliente);
					saldoParaBloqueio = 0;
				} else {
					produtosBloqueadosCliente = copiarProdutosCliente(produtosCliente);
					saldoParaBloqueio -= produtosCliente.getSaldoProduto();
					double salodBloqueado = produtosCliente.getSaldoProduto();
					produtosBloqueadosCliente.setSaldoProduto(salodBloqueado);
					produtosClienteBloqueados.add(produtosBloqueadosCliente);
					produtosCliente.setSaldoProduto(0);
					produtosClienteService.atualizarProdutosCliente(produtosCliente);
				}
			}
		}

		return produtosClienteBloqueados;
	}
	
	private ProdutosClienteBloqueados copiarProdutosCliente(ProdutosCliente produtosCliente) {
		ProdutosClienteBloqueados produtosClienteBloqueados = new ProdutosClienteBloqueados();
		
		produtosClienteBloqueados.setCpfCliente(produtosCliente.getCpfCliente());
		produtosClienteBloqueados.setIdProdutoCliente(produtosCliente.getIdProdutoCliente());
		produtosClienteBloqueados.setProduto(produtosCliente.getProduto());
		produtosClienteBloqueados.setSaldoProduto(produtosCliente.getSaldoProduto());
		
		return produtosClienteBloqueados;
	}
	
	public double calcularTotalSaldo(List<ProdutosClienteBloqueados> produtosCliente) {

		double valorTotalBloqueado = 0;

		for (ProdutosClienteBloqueados produtosClientes : produtosCliente) {
			valorTotalBloqueado = valorTotalBloqueado + produtosClientes.getSaldoProduto();
		}
		return valorTotalBloqueado;

	}
	
	public List<ProdutoBloqueado> montarListaProdutosBloqueado(Bloqueio bloqueio,
			List<ProdutosClienteBloqueados> produtosCliente) {

		List<ProdutoBloqueado> produtosBloqueados = new ArrayList<>();

		for (ProdutosClienteBloqueados produtosClientes : produtosCliente) {
			ProdutoBloqueado produtoBloqueado = new ProdutoBloqueado();
			produtoBloqueado.setBloqueio(bloqueio);
			produtoBloqueado.setValorBloqueado(produtosClientes.getSaldoProduto());
			produtoBloqueado.setProdutosCliente(produtosClienteService.consultaSaldoPorId(produtosClientes.getIdProdutoCliente()).get());
			produtosBloqueados.add(produtoBloqueado);
		}

		return produtosBloqueados;
	}
	
}
