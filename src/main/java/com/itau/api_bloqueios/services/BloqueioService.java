package com.itau.api_bloqueios.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itau.api_bloqueios.models.Bloqueio;
import com.itau.api_bloqueios.models.ProdutoBloqueado;
import com.itau.api_bloqueios.models.ProdutosCliente;
import com.itau.api_bloqueios.repositories.BloqueioRepository;

import br.com.itau.api_bloqueios.helpers.MontarProdutosBloqueado;
import br.com.itau.api_bloqueios.helpers.ProdutosClienteBloqueados;
import br.com.itau.api_bloqueios.helpers.RetornoSolicitacaoBloqueio;
import br.com.itau.api_bloqueios.helpers.SolicitacaoBDT;

@Service
public class BloqueioService {

	@Autowired
	BloqueioRepository bloqueioRepository;

	@Autowired
	ProdutosClienteService produtosClienteService;

	@Autowired
	ProdutoBloqueadoService produtosBloqueadoService;
	
	@Autowired
	ProcessaBloqueioService processaBloqueioService;

	public RetornoSolicitacaoBloqueio solicitarBloqueio(SolicitacaoBDT solicitacaoBDT) {

		List<ProdutosCliente> produtoCliente = produtosClienteService.consultaSaldo(solicitacaoBDT.getCpfReu());

//		if (!(produtoCliente.isEmpty())) {
			List<ProdutosClienteBloqueados> produtosClienteBloqueado = processaBloqueioService.gerarBloqueio(produtoCliente,
					solicitacaoBDT.getValorSolicitado());
			double saldoTotalBloqueado = processaBloqueioService.calcularTotalSaldo(produtosClienteBloqueado);
			Bloqueio bloqueio = salvarBloqueio(solicitacaoBDT, saldoTotalBloqueado);
			MontarProdutosBloqueado montarProdutosBloqueados = new MontarProdutosBloqueado();
			List<ProdutoBloqueado> produtosBloqueado = processaBloqueioService.montarListaProdutosBloqueado(bloqueio, produtosClienteBloqueado);
			produtosBloqueadoService.gravarListaBloqueio(produtosBloqueado);
			RetornoSolicitacaoBloqueio retorno = montarProdutosBloqueados.montarRetorno(bloqueio,produtosBloqueado);
			
			return retorno;
//		}
//		
//		return null;
		
	}

	private Bloqueio salvarBloqueio(SolicitacaoBDT solicitacaoBDT, double saldoTotalBloqueado) {
		Bloqueio bloqueio = new Bloqueio();

		bloqueio.setBloqueioAndamento(true);
		bloqueio.setCpfReu(solicitacaoBDT.getCpfReu());
		bloqueio.setNumeroOrdem(solicitacaoBDT.getNumeroOrdem());
		bloqueio.setValorSolicitadoBloqueio(solicitacaoBDT.getValorSolicitado());
		bloqueio.setValorBloqueado(saldoTotalBloqueado);

		bloqueioRepository.save(bloqueio);

		return bloqueio;

	}
	
	public Optional<Bloqueio> consultaBloqueio(int NumeroOrdem) {
		return bloqueioRepository.findById(NumeroOrdem);
	}
	
	public void alterarBloqueio(Bloqueio bloqueio) {
		bloqueioRepository.save(bloqueio);
	}
	
}
