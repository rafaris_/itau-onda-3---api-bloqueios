package com.itau.api_bloqueios.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itau.api_bloqueios.models.ProdutosCliente;
import com.itau.api_bloqueios.repositories.ProdutosClienteRepository;

@Service
public class ProdutosClienteService {
//	private double saldoFaltante;

	@Autowired
	ProdutosClienteRepository produtosClienteRepository;

	public List<ProdutosCliente> consultaSaldo(int cpfCliente) {
		return produtosClienteRepository.findAllBycpfCliente(cpfCliente);
	}
	
	public Optional<ProdutosCliente> consultaSaldoPorId(int idProdutoCliente) {
		return produtosClienteRepository.findById(idProdutoCliente);
	}

//	public List<ProdutosCliente> consomeSaldo(int cpfCliente, double saldoParaBloqueio) {
//		List<ProdutosCliente> listaProdutosBloqueados = new ArrayList<>();
//		List<ProdutosCliente> listaProdutosCliente = new ArrayList<>();
//
//		listaProdutosCliente = consultaSaldo(cpfCliente);
//		saldoFaltante = saldoParaBloqueio;
//
//		if (!(listaProdutosCliente.isEmpty())) {
//			for (ProdutosCliente produtosCliente : listaProdutosCliente) {
//
//				if (saldoFaltante > 0) {
//					if (produtosCliente.getSaldoProduto() >= saldoFaltante) {
//						produtosCliente.setSaldoProduto(produtosCliente.getSaldoProduto() - saldoFaltante);
//						atualizarProdutosCliente(produtosCliente);
//						produtosCliente.setSaldoProduto(saldoFaltante);
//						listaProdutosBloqueados.add(produtosCliente);
//						saldoFaltante = 0;
//					} else {
//						saldoFaltante -= produtosCliente.getSaldoProduto();
//						listaProdutosBloqueados.add(produtosCliente);
//						produtosCliente.setSaldoProduto(0);
//						atualizarProdutosCliente(produtosCliente);
//					}
//
//				}
//			}
//
//		}
//
//		if (listaProdutosBloqueados.isEmpty()) {
//			return null;
//		} else {
//			return listaProdutosBloqueados;
//		}
//	}
	
	public void atualizarProdutosCliente(ProdutosCliente produtosCliente) {
		produtosClienteRepository.save(produtosCliente);
	}
}