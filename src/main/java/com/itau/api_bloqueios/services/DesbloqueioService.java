package com.itau.api_bloqueios.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itau.api_bloqueios.models.Bloqueio;
import com.itau.api_bloqueios.models.ProdutoBloqueado;
import com.itau.api_bloqueios.models.ProdutosCliente;

import br.com.itau.api_bloqueios.helpers.SolicitacaoBDT;

@Service
public class DesbloqueioService {
	private double valorADesbloquear;

	@Autowired
	ProdutoBloqueadoService produtoBloqueadoService;

	@Autowired
	ProdutosClienteService produtosClienteService;

	@Autowired
	BloqueioService bloqueioService;
	
	public void desbloquearProduto(SolicitacaoBDT solicitacaoBDT) {
		List<ProdutoBloqueado> produtosBloqueado = new ArrayList<>();

		valorADesbloquear = solicitacaoBDT.getValorSolicitado();
		Bloqueio bloqueio = bloqueioService.consultaBloqueio(solicitacaoBDT.getNumeroOrdem()).get();
		double novoValorBloqueado = bloqueio.getValorBloqueado();
		produtosBloqueado = produtoBloqueadoService.consultarBloqueio(bloqueio);

		
		for (ProdutoBloqueado produtosBloqueados : produtosBloqueado) {
			
			if(valorADesbloquear > 0) {
				if (valorADesbloquear >= produtosBloqueados.getValorBloqueado()) {
					ProdutosCliente produtosCliente = new ProdutosCliente();
					produtosCliente = consultaProdutoCliente(produtosBloqueados.getProdutosCliente().getIdProdutoCliente());
					produtosCliente.setSaldoProduto(produtosBloqueados.getValorBloqueado() + produtosCliente.getSaldoProduto());
					produtosClienteService.atualizarProdutosCliente(produtosCliente);
					valorADesbloquear -= produtosBloqueados.getValorBloqueado();
					novoValorBloqueado -= produtosBloqueados.getValorBloqueado();
					produtosBloqueados.setValorBloqueado(0);
					produtoBloqueadoService.atualizarSaldo(produtosBloqueados);	
				}else {
					ProdutosCliente produtosCliente = new ProdutosCliente();
					produtosBloqueados.setValorBloqueado(produtosBloqueados.getValorBloqueado() - valorADesbloquear);
					produtosCliente = consultaProdutoCliente(produtosBloqueados.getProdutosCliente().getIdProdutoCliente());
					produtosCliente.setSaldoProduto(valorADesbloquear + produtosCliente.getSaldoProduto());
					produtosClienteService.atualizarProdutosCliente(produtosCliente);
					novoValorBloqueado -= valorADesbloquear;
					valorADesbloquear -= valorADesbloquear;
					produtoBloqueadoService.atualizarSaldo(produtosBloqueados);	
				}
				
			}
		}
		if (novoValorBloqueado == 0) {
			bloqueio.setBloqueioAndamento(false);
		}
		
		bloqueio.setValorBloqueado(novoValorBloqueado);
		bloqueioService.alterarBloqueio(bloqueio);
	}
	
	private ProdutosCliente consultaProdutoCliente(int id) {
		ProdutosCliente produtoCliente = new ProdutosCliente();
		produtoCliente = produtosClienteService.consultaSaldoPorId(id).get();
		
		return produtoCliente;
	}
}
	