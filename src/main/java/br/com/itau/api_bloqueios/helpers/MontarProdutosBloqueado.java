package br.com.itau.api_bloqueios.helpers;

import java.util.ArrayList;
import java.util.List;

import com.itau.api_bloqueios.models.Bloqueio;
import com.itau.api_bloqueios.models.ProdutoBloqueado;



public class MontarProdutosBloqueado {

	public RetornoSolicitacaoBloqueio montarRetorno(Bloqueio bloqueio, List<ProdutoBloqueado> produtosBloqueado) {
		RetornoSolicitacaoBloqueio retorno = new RetornoSolicitacaoBloqueio();
		retorno.setBloqueio(bloqueio);
		
		List<ProdutosLista> produtoLista = new ArrayList<>();
		
		for (ProdutoBloqueado produtosBloqueados : produtosBloqueado) {
			ProdutosLista produtosLista = new ProdutosLista();
			produtosLista.setCodigoProduto(produtosBloqueados.getProdutosCliente().getProduto().getIdProduto());
			produtosLista.setNomeProduto(produtosBloqueados.getProdutosCliente().getProduto().getNomeProduto());
			produtosLista.setSaldoProduto(produtosBloqueados.getProdutosCliente().getSaldoProduto());
			produtosLista.setValorBloqueado(produtosBloqueados.getValorBloqueado());
			produtoLista.add(produtosLista);
		}
			
		retorno.setProdutosLista(produtoLista);
		
		return retorno;
	}

}
