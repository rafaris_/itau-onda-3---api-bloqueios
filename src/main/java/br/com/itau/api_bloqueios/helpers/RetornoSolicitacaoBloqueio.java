package br.com.itau.api_bloqueios.helpers;

import java.util.List;

import com.itau.api_bloqueios.models.Bloqueio;

public class RetornoSolicitacaoBloqueio {

	private Bloqueio bloqueio;
	private List<ProdutosLista> produtosLista;

	public Bloqueio getBloqueio() {
		return bloqueio;
	}

	public void setBloqueio(Bloqueio bloqueio) {
		this.bloqueio = bloqueio;
	}

	public List<ProdutosLista> getProdutosLista() {
		return produtosLista;
	}

	public void setProdutosLista(List<ProdutosLista> produtosLista) {
		this.produtosLista = produtosLista;
	}

}
