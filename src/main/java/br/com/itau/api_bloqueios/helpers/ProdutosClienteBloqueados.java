package br.com.itau.api_bloqueios.helpers;

import com.itau.api_bloqueios.models.Produto;

public class ProdutosClienteBloqueados {

	private int idProdutoCliente;
	private double saldoProduto;
	private int cpfCliente;
	private Produto produto;

	public int getIdProdutoCliente() {
		return idProdutoCliente;
	}

	public void setIdProdutoCliente(int idProdutoCliente) {
		this.idProdutoCliente = idProdutoCliente;
	}

	public double getSaldoProduto() {
		return saldoProduto;
	}

	public void setSaldoProduto(double saldoProduto) {
		this.saldoProduto = saldoProduto;
	}

	public int getCpfCliente() {
		return cpfCliente;
	}

	public void setCpfCliente(int cpfCliente) {
		this.cpfCliente = cpfCliente;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

}
