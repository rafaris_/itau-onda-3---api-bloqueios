package br.com.itau.api_bloqueios.helpers;

public class ProdutosLista {

	private int codigoProduto;
	private String nomeProduto;
	private double saldoProduto;
	private double valorBloqueado;

	public int getCodigoProduto() {
		return codigoProduto;
	}

	public void setCodigoProduto(int codigoProduto) {
		this.codigoProduto = codigoProduto;
	}

	public String getNomeProduto() {
		return nomeProduto;
	}

	public void setNomeProduto(String nomeProduto) {
		this.nomeProduto = nomeProduto;
	}

	public double getSaldoProduto() {
		return saldoProduto;
	}

	public void setSaldoProduto(double saldoProduto) {
		this.saldoProduto = saldoProduto;
	}

	public double getValorBloqueado() {
		return valorBloqueado;
	}

	public void setValorBloqueado(double valorBloqueado) {
		this.valorBloqueado = valorBloqueado;
	}
}
