package br.com.itau.api_bloqueios.helpers;

public class SolicitacaoBDT {

	private int numeroOrdem;
	private int cpfReu;
	private double valorSolicitado;

	public int getNumeroOrdem() {
		return numeroOrdem;
	}

	public void setNumeroOrdem(int numeroOrdem) {
		this.numeroOrdem = numeroOrdem;
	}

	public int getCpfReu() {
		return cpfReu;
	}

	public void setCpfReu(int cpfReu) {
		this.cpfReu = cpfReu;
	}

	public double getValorSolicitado() {
		return valorSolicitado;
	}

	public void setValorSolicitado(double valorSolicitado) {
		this.valorSolicitado = valorSolicitado;
	}

}
