CREATE TABLE produto (
    id_produto INT (11) AUTO_INCREMENT PRIMARY KEY,
    nome_produto VARCHAR (25) NOT NULL,
    prioridade_produto INT (11) NOT NULL
);
 
CREATE TABLE produtoscliente (
    id_produto_cliente INT (11) AUTO_INCREMENT PRIMARY KEY,
    cpf_cliente BIGINT (11) NOT NULL,
    saldo_produto DECIMAL (8,2) NOT NULL,
    id_produto INT (11) NOT NULL,
    FOREIGN KEY (id_produto) REFERENCES produto (id_produto) ON DELETE CASCADE ON UPDATE CASCADE
);
 
CREATE TABLE bloqueio (
    numero_ordem BIGINT (11) PRIMARY KEY,
    cpf_reu BIGINT (11) NOT NULL,
    valor_solicitado_bloqueio DECIMAL (8,2) NOT NULL,
    valor_bloqueado DECIMAL (8,2) NOT NULL,
    bloqueio_andamento BOOLEAN NOT NULL
);

CREATE TABLE produtobloqueado (
    id_produto_bloqueado INT (11) AUTO_INCREMENT PRIMARY KEY,
    valor_bloqueado_produto DECIMAL (8,2) NOT NULL,
    id_produto_cliente INT (11) NOT NULL,
    numero_ordem BIGINT (11) NOT NULL,
    FOREIGN KEY (id_produto_cliente) REFERENCES produtoscliente (id_produto_cliente) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (numero_ordem) REFERENCES bloqueio (numero_ordem) ON DELETE CASCADE ON UPDATE CASCADE    
);  